// Headers
#include <windows.h>
#include <Winuser.h>
#include <tchar.h>
#include <commctrl.h>
#include "resource.h"
#include<ShellScalingApi.h>
#include <conio.h>


// Libs
#pragma comment(lib, "comctl32.lib")

// Various consts & Defs
#define	WM_USER_SHELLICON WM_USER + 1
#define WM_TASKBAR_CREATE RegisterWindowMessage(_T("TaskbarCreated"))

// Globals
HWND hWnd;
HINSTANCE hInst;
NOTIFYICONDATA structNID;
BOOL Enabled;
RECT monitorRects[2];
UINT dpis[2];
int monitorIdx = 0;
int pointerJumpAtY = 1073;
int xPos = 0, yPos = 0;
int threshold = 5;
int scale = 1;
int jumpToY = 0;
int jumpToX = 0;
int mouseSpeedX = 0;
int prevX, prevY;
float monScales[2];
int delay = 3;
int interval = 0;
bool jumped = false;
bool consolePresent = false;
LRESULT CALLBACK MouseHookProc(int nCode, WPARAM wParam, LPARAM lParam);
bool exitFlag = false;
template<typename... Args> void print(const char * f, Args... args) {
	if(consolePresent)
		_cprintf(f, args...);
}
/*
Name: ... DiagnosticsDlgProc(...)
Desc: proccess the about dialog
*/
BOOL CALLBACK DiagnosticsDlgProc(HWND hwnd, UINT Message, WPARAM wParam, LPARAM lParam)
{
    switch(Message)
    {
        case WM_INITDIALOG:
			return TRUE;
			break;
        case WM_COMMAND:
			switch(LOWORD(wParam))
			{
				case IDOK:
					EndDialog(hwnd, IDOK);
					break;
			}
			break;
        default:
            return FALSE;
    }
    return TRUE;
}

/* ================================================================================================================== */

/*
Name: ... WndProc(...)
Desc: Main hidden "Window" that handles the messaging for our system tray
*/
LRESULT CALLBACK WndProc(HWND hwnd, UINT Message, WPARAM wParam, LPARAM lParam)
{
	POINT lpClickPoint;

	if(Message == WM_TASKBAR_CREATE) {			// Taskbar has been recreated (Explorer crashed?)
		// Display tray icon
		if(!Shell_NotifyIcon(NIM_ADD, &structNID)) {
			MessageBox(NULL, "Systray Icon Creation Failed!", "Error!", MB_ICONEXCLAMATION | MB_OK);
			DestroyWindow(hWnd);
			return -1;
		}
	}

	switch(Message)
	{
		case WM_DESTROY:
			Shell_NotifyIcon(NIM_DELETE, &structNID);	// Remove Tray Item
			PostQuitMessage(0);							// Quit
			break;
		
		case WM_USER_SHELLICON:			// sys tray icon Messages
			switch(LOWORD(lParam))
			{
				case WM_RBUTTONDOWN:
					{
						HMENU hMenu, hSubMenu;
						// get mouse cursor position x and y as lParam has the Message itself
						GetCursorPos(&lpClickPoint);

						// Load menu resource
						hMenu = LoadMenu(hInst, MAKEINTRESOURCE(IDR_POPUP_MENU));
						if(!hMenu)
							return -1;	// !0, message not successful?

						// Select the first submenu
						hSubMenu = GetSubMenu(hMenu, 0);
						if(!hSubMenu) {
							DestroyMenu(hMenu);        // Be sure to Destroy Menu Before Returning
							return -1;
						}

						// Set Enabled State
						if(Enabled)
							CheckMenuItem(hMenu, ID_POPUP_ENABLE, MF_BYCOMMAND | MF_CHECKED);
						else
							CheckMenuItem(hMenu, ID_POPUP_ENABLE, MF_BYCOMMAND | MF_UNCHECKED);

						// Display menu
						SetForegroundWindow(hWnd);
						TrackPopupMenu(hSubMenu, TPM_LEFTALIGN | TPM_LEFTBUTTON | TPM_BOTTOMALIGN, lpClickPoint.x, lpClickPoint.y, 0, hWnd, NULL);
						SendMessage(hWnd, WM_NULL, 0, 0);

						// Kill off objects we're done with
						DestroyMenu(hMenu);
					}
					break;
			}
			break;
		case WM_CLOSE:
				DestroyWindow(hWnd);	// Destroy Window
				exitFlag = true;
				break;
		case WM_COMMAND:
			switch(LOWORD(wParam))
			{
				case ID_POPUP_EXIT:
					DestroyWindow(hWnd);		// Destroy Window
					break;
//				case ID_POPUP_DIAGNOSTICS:			// Open about box
//					//DialogBox(GetModuleHandle(NULL), MAKEINTRESOURCE(IDD_ABOUT), hwnd, (DLGPROC)DiagnosticsDlgProc);
//#ifdef _DEBUG
//					print("Show Console\n");
//#else
//					if (!AllocConsole())
//						MessageBox(NULL, "Failed to create the console!", "Error!", MB_ICONEXCLAMATION | MB_OK);
//					else{
//						print("Console created\n");
//						consolePresent = true;
//					}
//#endif
//					break;
				case ID_POPUP_ENABLE:			// Toggle Enable
					Enabled = !Enabled;
					break;
			}
			break;
		default:
			return DefWindowProc(hwnd, Message, wParam, lParam);
	}
	return 0;		// Return 0 = Message successfully proccessed
}


LRESULT CALLBACK MouseHookProc(int nCode, WPARAM wParam, LPARAM lParam) {
	//print("interval = %d\n", interval);
	if (nCode >= 0 && wParam == WM_MOUSEMOVE) {
		MSLLHOOKSTRUCT* mh = (MSLLHOOKSTRUCT*)lParam;
		mouseSpeedX = mh->pt.x - prevX;
		//print("%d , %d ? mouseSpeedX = %d\n", mh->pt.x, mh->pt.y, mouseSpeedX);
		
		//If mouse x reaches mon0.right-thresh, interpolate its mouse y to mon0.bottom (height)
		if (mh->pt.x > scale*monitorRects[0].right - threshold && mh->pt.x < scale*monitorRects[0].right + threshold)
		{
			if (!jumped) {
				if (mouseSpeedX >= 0){
					jumpToY = int(monScales[0] * mh->pt.y);
					jumpToX = mh->pt.x + threshold*2;
				}
				else{
					jumpToY = int(monScales[1] * mh->pt.y);
					jumpToX = mh->pt.x - threshold * 2;
				}
				
				//print("In jump region at %d,%d.\n", mh->pt.x, mh->pt.y);
				PostThreadMessage(GetCurrentThreadId(), WM_USER, 0, 0);
				jumped = true;
			}
			/*else
			{
				if (++interval > delay) interval = 0;
			}*/
		}
		else 
		{
			jumped = false;
			interval = 0;
		}
		prevX = mh->pt.x;
		prevY = mh->pt.y;
		
	}
	return CallNextHookEx(NULL, nCode, wParam, lParam);
}
BOOL CALLBACK MyInfoEnumProc(HMONITOR monitor,	HDC hdc, LPRECT lprect, LPARAM lparam)
{
	MONITORINFO mi;
	RECT rc;
	UINT dpiX, dpiY;

	//Get screen sizes
	mi.cbSize = sizeof(mi);
	GetMonitorInfo(monitor, &mi);
	rc = mi.rcMonitor;
	monitorRects[monitorIdx] = rc;
	
	//Get dpis for scaling
	HRESULT dpiInfo = GetDpiForMonitor(monitor, MONITOR_DPI_TYPE::MDT_EFFECTIVE_DPI, &dpiX, &dpiY);
	dpis[monitorIdx] = dpiX;
	monitorIdx++;
	//print("DPI : %d,%d\n", dpiX, dpiY);
	
	return true;
}
/*
Name: ... WinMain(...)
Desc: Main Entry point
*/

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
	// adding hook
	HHOOK mouseHook = SetWindowsHookEx(WH_MOUSE_LL, MouseHookProc, hInstance, NULL);

	MSG Msg;
	WNDCLASSEX wc;
	HANDLE hMutexInstance;
	INITCOMMONCONTROLSEX iccex;
	//POINT cursor;
	
	// Check for single instance
	// ------------------------------
	// Note: I recommend to use the GUID Creation Tool for the most unique id
	// Tools->Create GUID for Visual Studio .Net 2003
	// Or search somewhere in the Platform SDK for other environments
	hMutexInstance = CreateMutex(NULL, FALSE,_T("TrayApp-{1EB489D6-6702-43cd-A859-C2BA7DB58B06}"));
	if(GetLastError() == ERROR_ALREADY_EXISTS || GetLastError() == ERROR_ACCESS_DENIED)
		return 0;

	// Copy instance so it can be used globally in other methods
	hInst = hInstance;

	// Init common controls (if you're using them)
	// ------------------------------
	// See: http://msdn.microsoft.com/library/default.asp?url=/library/en-us/shellcc/platform/commctls/common/structures/initcommoncontrolsex.asp
	iccex.dwSize = sizeof(INITCOMMONCONTROLSEX);
	iccex.dwICC = ICC_UPDOWN_CLASS | ICC_LISTVIEW_CLASSES;
	if(!InitCommonControlsEx(&iccex)) {
		MessageBox(NULL, "Cannot Initialize Common Controls!", "Error!", MB_ICONEXCLAMATION | MB_OK);
		return 0;
	}

	// Window "class"
	wc.cbSize =			sizeof(WNDCLASSEX);
	wc.style =			CS_HREDRAW | CS_VREDRAW;
	wc.lpfnWndProc =	WndProc;
	wc.cbClsExtra =		0;
	wc.cbWndExtra =		0;
	wc.hInstance =		hInstance;
	wc.hIcon =			LoadIcon(hInstance, (LPCTSTR)MAKEINTRESOURCE(IDI_TRAYICON));
	wc.hCursor =		LoadCursor(NULL, IDC_ARROW);
	wc.hbrBackground =	(HBRUSH)GetStockObject(WHITE_BRUSH);
	wc.lpszMenuName =	NULL;
	wc.lpszClassName =	"Mouse Mapper";
	wc.hIconSm		 =	LoadIcon(hInstance, (LPCTSTR)MAKEINTRESOURCE(IDI_TRAYICON));
	if(!RegisterClassEx(&wc)) {
		MessageBox(NULL, "Window Registration Failed!", "Error!", MB_ICONEXCLAMATION | MB_OK);
		return 0;
	}

	//Create the Console
#ifdef _DEBUG
	if (!AllocConsole())
		MessageBox(NULL, "Failed to create the console!", "Error!", MB_ICONEXCLAMATION | MB_OK);
	else {
		consolePresent = true;
		print("Console created\n");
	}
		
#endif

	// Create the hidden window
	hWnd = CreateWindowEx(
		WS_EX_CLIENTEDGE,
		"Mouse Mapper",
		"Mouse Mapper Framework",
		WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		CW_USEDEFAULT,
		NULL,
		NULL,
		hInstance,
		NULL);
	if(hWnd == NULL) {
		MessageBox(NULL, "Window Creation Failed!", "Error!", MB_ICONEXCLAMATION | MB_OK);
		return 0;
	}

	// tray icon settings
	structNID.cbSize = sizeof(NOTIFYICONDATA);
	structNID.hWnd = (HWND)hWnd;
	structNID.uID = IDI_TRAYICON;
	structNID.uFlags = NIF_ICON | NIF_MESSAGE | NIF_TIP;
	strcpy_s(structNID.szTip, "Mouse Mapper");
	structNID.hIcon = LoadIcon(hInstance, (LPCTSTR)MAKEINTRESOURCE(IDI_TRAYICON));
	structNID.uCallbackMessage = WM_USER_SHELLICON;

	// Display tray icon
	if(!Shell_NotifyIcon(NIM_ADD, &structNID)) {
		MessageBox(NULL, "Systray Icon Creation Failed!", "Error!", MB_ICONEXCLAMATION | MB_OK);
		return 0;
	}

	// Set mode to enabled
	Enabled = TRUE;

	// Message Loop
	/*while(GetMessage(&Msg, NULL, 0, 0))
	{
		TranslateMessage(&Msg);
		DispatchMessage(&Msg);
		print("Get message\n");
	}*/
	// Any available pointing and display devices
	// if debugLevel > 0, the list of available devices
	// and extended information will be output.
	/*PointingDevice *input = PointingDevice::create("any:?debugLevel=1");
	DisplayDevice *output = DisplayDevice::create("any:?debugLevel=1");

	func = TransferFunction::create("sigmoid:?debugLevel=2", input, output);*/

	// To receive events from PointingDevice object, a callback function must be set.
	//input->setPointingCallback(pointingCallback);
	//AllocConsole();
	consolePresent = true;
	
	EnumDisplayMonitors(NULL, NULL, MyInfoEnumProc, 0); 
	scale = dpis[0] / dpis[1];
	monScales[0] = float(monitorRects[1].bottom) / monitorRects[0].bottom;
	monScales[1] = float(monitorRects[0].bottom / monitorRects[1].bottom);
	/*char msg[50];
	itoa(dpis[1], msg, 10);
	MessageBox(NULL, msg, "Info", MB_ICONEXCLAMATION | MB_OK);*/
	
	//print("%d/%d = %f", monitorRects[1].bottom , monitorRects[0].bottom, monScales[0]);
	//MessageBox(NULL, msg, "Info", MB_ICONEXCLAMATION | MB_OK);
	/*for (int i = 0; i < 2; i++) {
		print("%d,%d,%d,%d\n", monitorRects[i].left, monitorRects[i].top, monitorRects[i].right, monitorRects[i].bottom);
		print("%d > %d , scale by %f\n", i+1, i+2, monScales[i]);
	}*/
	
	while (!exitFlag)
	{
		if (GetMessage(&Msg, NULL, 0, 0))
		{
			if (Enabled) {
				
				if (Msg.message == WM_USER) {
					print("Got jump message, jump to %d,%d\n",jumpToX,jumpToY);
					SetCursorPos(jumpToX, jumpToY);
				}
				else {
					TranslateMessage(&Msg);
					DispatchMessage(&Msg);
				}
			}
		}
		else
		{
			if (!UnhookWindowsHookEx(mouseHook)) {
				MessageBox(NULL, "Could not unregister mouse hook, please kill the app in the task manager!", "Warning!", MB_ICONEXCLAMATION | MB_OK);
			}
#ifdef _DEBUG
			print("Exiting console...\n");
			if (!FreeConsole())
				MessageBox(NULL, "Could not free console!", "Error!", MB_ICONEXCLAMATION | MB_OK);
			
#endif
			return 0;
		}
	}
	
	return int(Msg.wParam);
}
